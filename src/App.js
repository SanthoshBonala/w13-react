import React, { Component } from 'react';
import './App.css';

class App extends Component {
  constructor() {
    super();
    this.state = {
      phoneNumber: '660-562-1600'
    }
    this.handleChange = this.handleChange.bind(this);
    this.handleClick = this.handleClick.bind(this);
  }

  handleChange(evt) {
    let phoneNumber = evt.target.value;

    this.setState({ phoneNumber });

  }
  handleClick(evt) {
    let n = this.state.phoneNumber;
    var count = 0; var i = 0;
    for (i = 0; i < 12; i++) {
      if (n[i] === '-') {
        count++;
      }

    }
    let pattern = new RegExp(/[~`!#$%\^&*+=\\[\]\\';,/{}|\\":<>\?]/);
    if (count === 2 && n[3] === '-' && n[7] === '-' && n.length === 12 && !pattern.test(n)
      && !n.includes(".")) {
      if (Object.is(n.toLowerCase(), n.toUpperCase())) {
        window.alert("Legal PhoneNumber")
      } else {
        window.alert("illegal PhoneNumber")
      }

    } else {

      window.alert("illegal PhoneNumber")
    }

  }

  render() {
    return (
      <div className="App" >
        <input type="text" onChange={this.handleChange} value={this.state.phoneNumber} />
        <button type="button" name="verifyPhoneNumber" onClick={this.handleClick} >
          verifyPhoneNumber</button>
      </div>
    );
  }
}

export default App;
